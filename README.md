# Marley Spoon Recipes App Demo

### Development Machine Configuration
* macOS 10.14.5
* Xcode 11.0+
* iOS 11+
* Cocoapods 1.8.3+

Please run `pod install` first. Then open **MSContentfulClient.xcworkspace**.

#### Tests
I have added some tests under test target **MSContentfulClientTests** and **MSContentfulClientUITests**.