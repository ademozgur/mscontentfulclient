//
//  RecipeDetailsController.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import UIKit
import SDWebImage
import MarkdownKit

class RecipeDetailsController: UIViewController {

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var recipeTitle: UILabel!
    @IBOutlet private weak var recipeDescription: UITextView!
    @IBOutlet private weak var tags: UILabel!
    @IBOutlet private weak var chef: UILabel!
    
    private var viewModel: RecipeDetailsViewModel
    
    init(viewModel: RecipeDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "\(RecipeDetailsController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        recipeDescription.textContainerInset = UIEdgeInsets.zero
        recipeDescription.textContainer.lineFragmentPadding = 0
        
        imageView.sd_setImage(with: viewModel.recipe.photo?.url, completed: nil)
        recipeTitle.text = viewModel.recipe.title
        
        recipeDescription.attributedText = RecipeDetailsController.markdownParser.parse(viewModel.recipe.description ?? "")
        tags.text = "Tags: " + (viewModel.recipe.tags?.compactMap({ $0.name }).joined(separator: ", ") ?? "")
        chef.text = "Chef: " + (viewModel.recipe.chef?.name ?? "")
    }
    
    private static let markdownParser = MarkdownParser(font: UIFont.systemFont(ofSize: 17))

}
