//
//  Navigator.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import UIKit

class Navigator {
    
    public static let `default` = Navigator()
    
    private init() {}
}

extension Navigator: RecipeListControllerNavigationDelegate {
    
    func didSelectRecipe(sender: UIViewController, recipe: Recipe) {
        
        guard let viewModel = DependencyInjector.resolve(serviceType: RecipeDetailsViewModel.self,
                                                              argument: recipe) else { return }
        
        guard let viewController = DependencyInjector.resolve(serviceType: RecipeDetailsController.self, argument: viewModel) else { return }
        
        sender.navigationController?.pushViewController(viewController, animated: true)
        
    }
       
}

