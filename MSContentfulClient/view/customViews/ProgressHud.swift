//
//  ProgressHud.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import JGProgressHUD

public class ProgressHud: JGProgressHUD {
    
    private override init(style: JGProgressHUDStyle) {
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public class func createHud(style: JGProgressHUDStyle = .dark) -> ProgressHud {
        let jgprogressHUD = ProgressHud(style: style)
        jgprogressHUD.animation = JGProgressHUDFadeZoomAnimation()
        jgprogressHUD.position = .center
        
        if let indicatorView = jgprogressHUD.indicatorView as? JGProgressHUDIndeterminateIndicatorView {
            indicatorView.setColor(UIColor.white)
        }
        
        return jgprogressHUD
    }
}
