//
//  RecipeListController.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import UIKit
import ReactiveSwift
import JGProgressHUD

/// A navigation delegate to break the coupling between this controller and other controllers.
protocol RecipeListControllerNavigationDelegate: class {
    func didSelectRecipe(sender: UIViewController, recipe: Recipe)
}

class RecipeListController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!

    private let progressHud = ProgressHud.createHud()
    
    private let disposeBag = CompositeDisposable()
    
    private let viewModel: RecipeListViewModel
    
    private weak var navigationDelegate: RecipeListControllerNavigationDelegate?
    
    init(viewModel: RecipeListViewModel, navigationDelegate: RecipeListControllerNavigationDelegate?) {
        self.viewModel = viewModel
        self.navigationDelegate = navigationDelegate
        
        super.init(nibName: "\(RecipeListController.self)", bundle: nil)
        
        disposeBag += viewModel.recipes.signal.observeValues({ [weak self] recipes in
            guard let strongSelf = self else{ return }
            DispatchQueue.main.async {
                strongSelf.collectionView.reloadData()
            }
        })
        
        /// sometimes isBusy is triggered from background thread.
        /// make sure it is running on main thread.
        disposeBag += viewModel.isBusy.signal.observeValues({ [weak self] isBusy in
            guard let strongSelf = self else { return }
            
            DispatchQueue.main.async {
                if isBusy {
                    strongSelf.showProgress()
                    return
                }
                strongSelf.hideProgress()
            }
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        disposeBag.dispose()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
    }

    private lazy var doOnce: () -> Void = { [weak self] in
        guard let strongSelf = self else { return {} }
        strongSelf.disposeBag += strongSelf.viewModel.getRecipesAction.apply().start()
        return {}
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        doOnce()
    }
    
    private func showProgress() {
        if !progressHud.isVisible {
            progressHud.show(in: view)
        }
    }

    private func hideProgress() {
        progressHud.dismiss()
    }
    
    private func setupView() {
        let nib = UINib(nibName: "\(RecipeListCell.self)", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "\(RecipeListCell.self)")
    }
}


extension RecipeListController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.recipes.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(RecipeListCell.self)", for: indexPath)
        
        if let cellCasted = cell as? RecipeListCell {
            let recipe = viewModel.recipes.value[indexPath.row]
            cellCasted.bind(recipe: recipe)
        }
        
        return cell
    }
    
}

extension RecipeListController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let recipe = viewModel.recipes.value[indexPath.row]
        navigationDelegate?.didSelectRecipe(sender: self, recipe: recipe)
    }
    
}

extension RecipeListController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth: CGFloat = collectionView.bounds.size.width
        var remainingWidth = collectionViewWidth
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            remainingWidth = remainingWidth - layout.sectionInset.left - layout.sectionInset.right
        }
        
        let height: CGFloat = (remainingWidth * 9) / 16
        return CGSize(width: remainingWidth, height: height)
    }
}
