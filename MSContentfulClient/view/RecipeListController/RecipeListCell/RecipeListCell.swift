//
//  RecipeListCell.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import UIKit
import SDWebImage

class RecipeListCell: UICollectionViewCell {

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(recipe: Recipe) {
        label.text = recipe.title
        imageView.sd_setImage(with: recipe.photo?.url, completed: nil)
    }

}
