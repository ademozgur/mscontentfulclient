//
//  RecipeListViewModel.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class RecipeListViewModel {
    
    var disposeBag = CompositeDisposable()
    
    let receipeRepository: RecipeRepositoryProtocol
    
    let isBusy = MutableProperty<Bool>(false)
    
    let error = MutableProperty<AnyError?>(nil)
    
    let recipes = MutableProperty<[Recipe]>([])
    
    var getRecipesAction: Action<(), [Recipe], AnyError>
    
    init(receipeRepository: RecipeRepositoryProtocol) {
        self.receipeRepository = receipeRepository
        
        getRecipesAction = Action<(), [Recipe], AnyError>(execute: { _ -> SignalProducer<[Recipe], AnyError> in
            return receipeRepository.getReceipes()
        })
        
        disposeBag += recipes <~ getRecipesAction.values
        
        disposeBag += isBusy <~ getRecipesAction.isExecuting
    }
    
    deinit {
        disposeBag.dispose()
    }
    
}
