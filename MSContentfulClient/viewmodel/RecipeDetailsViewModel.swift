//
//  RecipeDetailsViewModel.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class RecipeDetailsViewModel {
    
    let recipe: Recipe
    
    init(recipe: Recipe) {
        self.recipe = recipe
    }
}
