//
//  ViewDependencyInjector.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import SwinjectAutoregistration
import SwinjectStoryboard
import Contentful

class DependencyInjector {
    
    private static var `default` = DependencyInjector()

    private let container = SwinjectStoryboard.defaultContainer

    private init() {
        registerModelLayerDependencies()
        registerViewModelLayerDependencies()
        registerViewLayerDependencies()
        
    }
    
    private func registerModelLayerDependencies() {
        container.autoregister(ContentfulClientProtocol.self) { _ -> ContentfulClientProtocol in
            let client = Client(spaceId: Constants.spaceId,
                                accessToken: Constants.accessToken,
                                contentTypeClasses: [Recipe.self, Chef.self, Tag.self])
            
            return client
        }
        
        container.autoregister(RecipeWebApi.self, initializer: RecipeWebApi.init)
        
        #if DEBUG
            if CommandLine.arguments.contains("isLaunchedFromUITestBundle") {
                container.autoregister(RecipeRepositoryProtocol.self, initializer: RecipeRepositorySampleDataProvider.init)
            } else {
                container.autoregister(RecipeRepositoryProtocol.self, initializer: RecipeRepository.init)
            }
        #else
            container.autoregister(RecipeRepositoryProtocol.self, initializer: RecipeRepository.init)
        #endif
    }
    
    private func registerViewModelLayerDependencies() {
        container.autoregister(RecipeListViewModel.self, initializer: RecipeListViewModel.init)
        container.autoregister(RecipeDetailsViewModel.self, argument: Recipe.self, initializer: RecipeDetailsViewModel.init)
    }
    
    private func registerViewLayerDependencies() {
        container.autoregister(RecipeListControllerNavigationDelegate.self) { _ -> RecipeListControllerNavigationDelegate in
            return Navigator.default
        }
        container.autoregister(RecipeListController.self, initializer: RecipeListController.init)
        container.autoregister(RecipeDetailsController.self, argument: RecipeDetailsViewModel.self, initializer: RecipeDetailsController.init)
    }
    
    
    /// Some class methods for easy access.
    
    public class func resolve<Service>(serviceType: Service.Type) -> Service? {
        return DependencyInjector.default.container.resolve(serviceType)
    }

    public class func resolve<Service, Arg1>(serviceType: Service.Type, argument: Arg1) -> Service? {
        return DependencyInjector.default.container.resolve(serviceType, argument: argument)
    }

    public class func resolve<Service, Arg1, Arg2>(serviceType: Service.Type,
                                                   arguments arg1: Arg1, arg2: Arg2) -> Service? {
        return DependencyInjector.default.container.resolve(serviceType, arguments: arg1, arg2)
    }
    
    public class func resolve<Service, Arg1, Arg2, Arg3>(serviceType: Service.Type,
                                                         arguments arg1: Arg1, arg2: Arg2, arg3: Arg3) -> Service? {
        return DependencyInjector.default.container.resolve(serviceType, arguments: arg1, arg2, arg3)
    }
}
