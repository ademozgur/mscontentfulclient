//
//  AppDelegate.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.        
        window = UIWindow(frame: UIScreen.main.bounds)
                
        if let viewController = DependencyInjector.resolve(serviceType: RecipeListController.self) {
            window?.rootViewController = UINavigationController(rootViewController: viewController)
        } else {
            fatalError("Unable to find rooViewController: \(RecipeListController.self)")
        }
        
        window?.makeKeyAndVisible()
        
        return true
    }


}

