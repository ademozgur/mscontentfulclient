//
//  ReceipeRepository.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result
import Contentful

protocol RecipeRepositoryProtocol {
    
    func getReceipes() -> SignalProducer<[Recipe], AnyError>
}

/// This class will work as a wrapper around various data providers like webapi, local cache or local database
/// For now it only has a webapi data provider.
class RecipeRepository: RecipeRepositoryProtocol {
    
    let recipeWebApi: RecipeWebApi
    
    init(recipeWebApi: RecipeWebApi) {
        self.recipeWebApi = recipeWebApi
    }
    
    func getReceipes() -> SignalProducer<[Recipe], AnyError> {
        return recipeWebApi.getReceipes()
    }
}

#if DEBUG
class RecipeRepositorySampleDataProvider: RecipeRepositoryProtocol {
    
    func getReceipes() -> SignalProducer<[Recipe], AnyError> {
        return SignalProducer<[Recipe], AnyError>(value: getSampleData())
    }
    
    func getSampleData() -> [Recipe] {
        
        let asset1 = getAsset()
        
        let recipe1 = Recipe(id: "123", updatedAt: nil, createdAt: nil, localeCode: nil, title: "Test Title",
                             photo: asset1, calories: 0, description: "Example description.", chef: getChef(), tags: [getTag()])
        
        return [recipe1]
    }
    
    private func getTag() -> Tag {
        let decoder = JSONDecoder()
        
        let localeStr = """
        {
          "code": "en-US",
          "name": "English (United States)",
          "default": true,
          "fallbackCode": null
        }
        """
        
        let str = """
        {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "kk2bw5ojx476"
              }
            },
            "id": "3TJp6aDAcMw6yMiE82Oy0K",
            "type": "Asset",
            "environment": {
              "sys": {
                "id": "master",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 1,
            "locale": "en-US"
          },
          "fields": {
            "name": "Test Tag"
          }
        }
        """
        
        do {
            
            let locale = try decoder.decode(Contentful.Locale.self, from: localeStr.data(using: .utf8)!)
            
            guard let localizationContext = LocalizationContext(locales: [locale]) else {
                fatalError("Unable to construct LocalizationContext.")
            }
            decoder.update(with: localizationContext)
            
            let asset = try decoder.decode(Tag.self, from: str.data(using: .utf8)!)
            return asset
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
    
    private func getChef() -> Chef {
        let decoder = JSONDecoder()
        
        let localeStr = """
        {
          "code": "en-US",
          "name": "English (United States)",
          "default": true,
          "fallbackCode": null
        }
        """
        
        let str = """
        {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "kk2bw5ojx476"
              }
            },
            "id": "3TJp6aDAcMw6yMiE82Oy0K",
            "type": "Asset",
            "environment": {
              "sys": {
                "id": "master",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 1,
            "locale": "en-US"
          },
          "fields": {
            "name": "Chef Name"
          }
        }
        """
        
        do {
            
            let locale = try decoder.decode(Contentful.Locale.self, from: localeStr.data(using: .utf8)!)
            
            guard let localizationContext = LocalizationContext(locales: [locale]) else {
                fatalError("Unable to construct LocalizationContext.")
            }
            decoder.update(with: localizationContext)
            
            let asset = try decoder.decode(Chef.self, from: str.data(using: .utf8)!)
            return asset
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
    
    private func getAsset() -> Asset {
        
        let decoder = JSONDecoder()
        
        let localeStr = """
        {
          "code": "en-US",
          "name": "English (United States)",
          "default": true,
          "fallbackCode": null
        }
        """
        
        let str = """
        {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "kk2bw5ojx476"
              }
            },
            "id": "3TJp6aDAcMw6yMiE82Oy0K",
            "type": "Asset",
            "environment": {
              "sys": {
                "id": "master",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 1,
            "locale": "en-US"
          },
          "fields": {
            "title": "SKU1503 Hero 102 1 -6add52eb4eec83f785974ddeac3316b7",
            "file": {
              "url": "//images.ctfassets.net/kk2bw5ojx476/3TJp6aDAcMw6yMiE82Oy0K/2a4cde3c1c7e374166dcce1e900cb3c1/SKU1503_Hero_102__1_-6add52eb4eec83f785974ddeac3316b7.jpg",
              "details": {
                "size": 216391,
                "image": {
                  "width": 1020,
                  "height": 680
                }
              },
              "fileName": "SKU1503_Hero_102__1_-6add52eb4eec83f785974ddeac3316b7.jpg",
              "contentType": "image/jpeg"
            },
            "description": "Recipe description"
          }
        }
        """
        
        do {
            let locale = try decoder.decode(Contentful.Locale.self, from: localeStr.data(using: .utf8)!)
            
            guard let localizationContext = LocalizationContext(locales: [locale]) else {
                fatalError("Unable to construct LocalizationContext.")
            }
            decoder.update(with: localizationContext)
            
            let asset = try decoder.decode(Asset.self, from: str.data(using: .utf8)!)
            return asset
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
}
#endif
