//
//  RecipeWebApi.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import Contentful
import ReactiveSwift
import Result

protocol ContentfulClientProtocol {
    
    @discardableResult
    func fetchArray<EntryType>(of entryType: EntryType.Type,
                                      matching query: QueryOn<EntryType>,
                                      then completion: @escaping ResultsHandler<HomogeneousArrayResponse<EntryType>>) -> URLSessionDataTask
    
    
}

extension Client: ContentfulClientProtocol {}

class RecipeWebApi {
    
    let client: ContentfulClientProtocol
    
    init(client: ContentfulClientProtocol) {
        self.client = client
    }
    
    func getReceipes() -> SignalProducer<[Recipe], AnyError> {
        
        return SignalProducer<[Recipe], AnyError> { [weak self] observer, _ in
            
            let query = QueryOn<Recipe>()
            
            self?.client.fetchArray(of: Recipe.self, matching: query) { result in
                switch result {
                case .success(let response):
                    let recipes = response.items
                    observer.send(value: recipes)
                    observer.sendCompleted()
                    
                case .error(let error):
                    observer.send(error: AnyError(error))
                }
            }
        }
    }
    
}
