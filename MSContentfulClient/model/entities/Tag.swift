//
//  Tag.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Contentful

final class Tag: EntryDecodable, FieldKeysQueryable {
    
    static let contentTypeId: ContentTypeId = "tag"
    
    let id: String
    let updatedAt: Date?
    let createdAt: Date?
    let localeCode: String?
    let name: String?
    
    enum FieldKeys: String, CodingKey {
        case name
    }
    
    required init(from decoder: Decoder) throws {
        let sys = try decoder.sys()
        id = sys.id
        localeCode = sys.locale
        updatedAt = sys.updatedAt
        createdAt = sys.createdAt
        
        let fields = try decoder.contentfulFieldsContainer(keyedBy: FieldKeys.self)
        self.name = try fields.decodeIfPresent(String.self, forKey: .name)
    }
    
    #if DEBUG
    init(id: String, updatedAt: Date?, createdAt: Date?, localeCode: String?, name: String?) {
        self.id = id
        self.updatedAt = updatedAt
        self.createdAt = createdAt
        self.localeCode = localeCode
        self.name = name
    }
    #endif
}
