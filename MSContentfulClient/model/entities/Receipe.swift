//
//  Receipe.swift
//  MSContentfulClient
//
//  Created by Adem Özgür on 30.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Contentful

final class Recipe: EntryDecodable, FieldKeysQueryable {
    
    static let contentTypeId: ContentTypeId = "recipe"
    
    let id: String
    let updatedAt: Date?
    let createdAt: Date?
    let localeCode: String?
    
    let title: String?
    private(set) var photo: Asset?
    let calories: Int?
    let description: String?
    private(set) var chef: Chef?
    private(set) var tags: [Tag]?
    
    enum FieldKeys: String, CodingKey {
        case title
        case photo
        case calories
        case description
        case chef
        case tags
    }
    
    required init(from decoder: Decoder) throws {
        let sys = try decoder.sys()
        id = sys.id
        localeCode = sys.locale
        updatedAt = sys.updatedAt
        createdAt = sys.createdAt
        
        let fields = try decoder.contentfulFieldsContainer(keyedBy: FieldKeys.self)
        self.title = try fields.decodeIfPresent(String.self, forKey: .title)
        self.calories = try fields.decodeIfPresent(Int.self, forKey: .calories)
        self.description = try fields.decodeIfPresent(String.self, forKey: .description)
        
        try fields.resolveLink(forKey: .photo, decoder: decoder, callback: { [weak self] result in
            self?.photo = result as? Asset
        })
        
        try fields.resolveLink(forKey: .chef, decoder: decoder, callback: { [weak self] result in
            self?.chef = result as? Chef
        })
        
        if decoder.canResolveLinks {
            try fields.resolveLinksArray(forKey: .tags, decoder: decoder, callback: { [weak self] result in
                self?.tags = result as? [Tag]
            })
        }
    }
    
    #if DEBUG
    init(id: String, updatedAt: Date?, createdAt: Date?, localeCode: String?,
         title: String?, photo: Asset?, calories: Int?, description: String?, chef: Chef?, tags: [Tag]?) {
        self.id = id
        self.updatedAt = updatedAt
        self.createdAt = createdAt
        self.localeCode = localeCode
        
        self.title = title
        self.photo = photo
        self.calories = calories
        self.description = description
        self.chef = chef
        self.tags = tags
    }
    #endif
}
