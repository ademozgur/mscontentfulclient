//
//  MSContentfulClientUITests.swift
//  MSContentfulClientUITests
//
//  Created by Adem Özgür on 1.11.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import XCTest

class MSContentfulClientUITests: XCTestCase {

    var app: XCUIApplication?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
        app = XCUIApplication()
        //app.launchEnvironment["isLaunchedFromUITestBundle"] = "true"
        app?.launchArguments = ["isLaunchedFromUITestBundle"]
        app?.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_recipeList() {
        // UI tests must launch the application that they test.
        
        guard let collectionViewsQuery = app?.collectionViews else {
            fatalError("There must be a collection view here!")
        }
        
        let cellCount = collectionViewsQuery.children(matching: .cell).count
        XCTAssert(cellCount == 1)
        
        let firstCell = collectionViewsQuery.children(matching: .cell).element(boundBy: 0)
        let firstTitleLabel = firstCell.descendants(matching: .staticText).matching(identifier: "recipeTitle").firstMatch
        
        XCTAssert(firstTitleLabel.label == "Test Title")
        
        let imageView = firstCell.descendants(matching: .image).matching(identifier: "recipeImageView").firstMatch
        XCTAssert(imageView.exists)
                
    }
    
    func test_recipeDetails() {
        
        guard let app = app else {
            fatalError("There must be an app here!")
        }
        
        // tap on first cell
        app.collectionViews.children(matching: .cell).element(boundBy: 0).images["recipeImageView"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        
        XCTAssert(elementsQuery.images["recipeImageView"].exists)
        
        XCTAssert(elementsQuery.staticTexts["recipeTitle"].exists)
        XCTAssert(elementsQuery.staticTexts["recipeTitle"].label == "Test Title")
        
        XCTAssert(elementsQuery.staticTexts["recipeTags"].exists)
        XCTAssert(elementsQuery.staticTexts["recipeTags"].label.hasSuffix("Test Tag"))
        
        XCTAssert(elementsQuery.textViews["recipeDescription"].exists)
        XCTAssert((elementsQuery.textViews["recipeDescription"].value as? String) == "Example description.")
        
        XCTAssert(elementsQuery.staticTexts["chefName"].exists)
        XCTAssert(elementsQuery.staticTexts["chefName"].label.hasSuffix("Chef Name"))
    }
}
