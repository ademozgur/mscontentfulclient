//
//  TestHelper.swift
//  MSContentfulClientTests
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
@testable import MSContentfulClient

// needed an internal type from Contentful SDK to mock the Contentful client
@testable import Contentful


class TestHelper {
    
    class func getLocale() -> Contentful.Locale {
        
        guard let filePath = Bundle(for: TestHelper.self).path(forResource: "Locale", ofType: "json") else {
            fatalError("Unable to find Locale.json file in test bundle.")
        }
        
        let fileUrl = URL(fileURLWithPath: filePath)
        
        
        let decoder = JSONDecoder()
        
        do {
            let fileContentData = try Data(contentsOf: fileUrl)
            let locale = try decoder.decode(Contentful.Locale.self, from: fileContentData)
            return locale
        } catch let error {
            fatalError("Unable to parse Locale.json file. error: " + error.localizedDescription)
        }
    }
    
    class func getMockedRecipes() -> HomogeneousArrayResponse<Recipe> {
        
        guard let filePath = Bundle(for: TestHelper.self).path(forResource: "Recipes", ofType: "json") else {
            fatalError("Unable to find Locale.json file in test bundle.")
        }
        
        let fileUrl = URL(fileURLWithPath: filePath)
        
        let fileContentData = try? Data(contentsOf: fileUrl)
        
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        let locale = TestHelper.getLocale()
        guard let localizationContext = LocalizationContext(locales: [locale]) else {
            fatalError("Unable to construct LocalizationContext.")
        }
        decoder.update(with: localizationContext)
        guard let linkResolverContextKey = CodingUserInfoKey(rawValue: "linkResolverContext") else {
            fatalError("Unable to construct linkResolverContextKey.")
        }
        decoder.userInfo[linkResolverContextKey] = LinkResolver()
        
        do {
            var contentTypes = [ContentTypeId: EntryDecodable.Type]()
            for type in [Recipe.self, Tag.self, Chef.self] as! [EntryDecodable.Type] {
                contentTypes[type.contentTypeId] = type
            }
            let contentTypesContextKey = CodingUserInfoKey(rawValue: "contentTypesContext")!
            decoder.userInfo[contentTypesContextKey] = contentTypes
            
            let homogeneousArray: HomogeneousArrayResponse<Recipe> = try decoder.decode(HomogeneousArrayResponse<Recipe>.self, from: fileContentData!)
            
            return homogeneousArray
            
        } catch let error {
            fatalError("Unable to parse Recipes.json file. error: " + error.localizedDescription)
        }
    }
}
