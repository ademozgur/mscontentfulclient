//
//  ContentfulClientMock.swift
//  MSContentfulClientTests
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import Contentful
@testable import MSContentfulClient

class ContentfulClientMock: ContentfulClientProtocol {
    
    var result: Any?
    
    init(result: Any?) {
        self.result = result
    }
    
    func fetchArray<EntryType>(of entryType: EntryType.Type, matching query: QueryOn<EntryType>,
                               then completion: @escaping (Result<HomogeneousArrayResponse<EntryType>>) -> Void) -> URLSessionDataTask
        where EntryType : EntryDecodable, EntryType : FieldKeysQueryable {
            
            /// TODO: find a better way of doing this.
            /// created this just to satify the mock requirement.
            let session = URLSessionMock()
            let dataTask = session.dataTask(with: URL(string: "http://google.com")!) { _, _, _ in
                completion(self.result as! Result<HomogeneousArrayResponse<EntryType>>)
            }
            dataTask.resume()
        return dataTask
    }
}
