//
//  RecipeWebApiTests.swift
//  MSContentfulClientTests
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import XCTest
import Contentful
import ReactiveSwift
@testable import MSContentfulClient

class RecipeWebApiTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    

    func test_getReceipes_success() {
        
        let recipesResponse = Result<HomogeneousArrayResponse<Recipe>>.success(TestHelper.getMockedRecipes())
        
        let client: ContentfulClientMock = ContentfulClientMock(result: recipesResponse)
        
        let recipeWebApi = RecipeWebApi(client: client)
        
        let expect = expectation(description: "Recipe count expectation")
        
        recipeWebApi.getReceipes().startWithResult { result in
            switch result {
            case .success(let recipes):
                XCTAssert(recipes.count == recipesResponse.value?.items.count)
                expect.fulfill()
            case .failure(_):
                expect.isInverted = true
                expect.fulfill()
            }
        }
        
        wait(for: [expect], timeout: 5)
    }
    
    func test_getReceipes_fail() {
        
        let error = NSError(domain: "", code: 0, userInfo: nil)
        
        let recipesResponse = Result<HomogeneousArrayResponse<Recipe>>(error: error)
        
        let client: ContentfulClientMock = ContentfulClientMock(result: recipesResponse)
        
        let recipeWebApi = RecipeWebApi(client: client)
        
        let expect = expectation(description: "Failure expectation")
        
        recipeWebApi.getReceipes().startWithResult { result in
            switch result {
            case .success(_):
                expect.isInverted = true
                expect.fulfill()
            case .failure(_):
                expect.fulfill()
            }
        }
        
        wait(for: [expect], timeout: 5)
    }

}
