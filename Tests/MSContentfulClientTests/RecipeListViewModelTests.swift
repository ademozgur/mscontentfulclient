//
//  RecipeListViewModelTests.swift
//  MSContentfulClientTests
//
//  Created by Adem Özgür on 31.10.2019.
//  Copyright © 2019 Home. All rights reserved.
//

import XCTest
import Contentful
import ReactiveSwift
@testable import MSContentfulClient

class RecipeListViewModelTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    /// viewModels recipeCount must be equal to mocked recipe count.
    func test_getRecipesAction() {
        
        let recipesResponse = Result<HomogeneousArrayResponse<Recipe>>.success(TestHelper.getMockedRecipes())
        
        let client: ContentfulClientMock = ContentfulClientMock(result: recipesResponse)
        
        let recipeWebApi = RecipeWebApi(client: client)
        
        let recipeRespository = RecipeRepository(recipeWebApi: recipeWebApi)
        
        let recipeListViewModel = RecipeListViewModel(receipeRepository: recipeRespository)
        
        let expect = expectation(description: "Recipe count expectation")
        
        recipeListViewModel.recipes.signal.observeValues { recipes in
            XCTAssert(recipes.count == recipesResponse.value?.items.count)
            expect.fulfill()
        }
        
        recipeListViewModel.getRecipesAction.apply().start()
        
        wait(for: [expect], timeout: 5)
    }

}
